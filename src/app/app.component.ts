import { Component, Inject, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile, KeycloakTokenParsed } from 'keycloak-js';
import { AppService } from "./app.service";
import { APP_CONFIG, AppConfig } from "../app.config";

type tokenType = "idTokenParsed"|"tokenParsed"|"serverTokenParsed"|"refreshTokenParsed"
type tokenRawType = "idToken"|"token"|"refreshToken"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'biletado';
  public isLoggedIn = false;
  public userProfile: KeycloakProfile | null = null;
  public dashboardUrls: {[index: string]: string} | undefined;
  public objectKeys = Object.keys; // this is used to access method from component
  jwtInfo: KeycloakTokenParsed | undefined;
  jwtInfoRaw: string | undefined;
  jwtInfoString: string = "";
  jwtInfoValidUntil: Date | undefined;
  jwtInfoCurrent: tokenType = "idTokenParsed";

  constructor(private readonly keycloak: KeycloakService, private appService: AppService, @Inject(APP_CONFIG) private appConfig: AppConfig) {}

  public async ngOnInit() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();
    this.dashboardUrls = this.appConfig.dashboardUrls;

    if (this.isLoggedIn) {
      this.userProfile = await this.keycloak.loadUserProfile();
    }
    this.showKCVariable(this.jwtInfoCurrent)
  }

  public login() {
    this.keycloak.login();
  }

  public logout() {
    this.keycloak.logout();
  }

  public updateToken(minValidity: number) {
    this.keycloak.updateToken(minValidity).then(() => {
      this.showKCVariable(this.jwtInfoCurrent);
    });
  }

  public showKCVariable(type: tokenType) {
    this.jwtInfoCurrent = type;
    if (type === "serverTokenParsed") {
      this.appService.getJWTInfo().subscribe((jwtInfo: {Claims?: KeycloakTokenParsed, Raw?: string}) => {
        this.jwtInfo = jwtInfo.Claims;
        this.jwtInfoRaw = jwtInfo.Raw;
        this.jwtInfoValidUntil = this.jwtInfo && this.jwtInfo.exp ? new Date(this.jwtInfo.exp * 1000) : undefined;
        this.jwtInfoString = JSON.stringify(this.jwtInfo, null, 4);
      });
      return;
    }
    this.jwtInfo = this.keycloak.getKeycloakInstance()[type];
    let rawType: tokenRawType = type.substring(0, type.length-6) as tokenRawType;
    this.jwtInfoRaw = this.keycloak.getKeycloakInstance()[rawType];
    this.jwtInfoValidUntil = this.jwtInfo && this.jwtInfo.exp ? new Date(this.jwtInfo.exp * 1000) : undefined;
    this.jwtInfoString = JSON.stringify(this.jwtInfo, null, 4);
  }
}
