FROM node:16-alpine as build
WORKDIR /app
COPY package.json ./
COPY yarn.lock ./
RUN yarn install
COPY ./ ./
RUN yarn build

FROM nginx:1.21-alpine
COPY --from=build /app/dist/biletado /usr/share/nginx/html
EXPOSE 80
